<?php
declare(strict_types=1);

namespace GitLab\Test\Projects;

use PHPUnit\Framework\TestCase;

class SnippetsTest extends TestCase
{
    use \GitLab\Test\GitLabTestTrait;

    public function testGetSnippets()
    {
        $client = $this->getClient();

        $project_id = getenv('CI_PROJECT_ID');

        $projects = new \GitLab\Projects\Snippets($client);
        $response = $projects->getSnippets($project_id);

        $this->assertEquals($response->getStatusCode(), 200);
    }

    public function testGetSnippet()
    {
        $client = $this->getClient();

        $project_id = getenv('CI_PROJECT_ID');
        $snippet_id = getenv('GITLAB_SNIPPET_ID');

        $projects = new \GitLab\Projects\Snippets($client);
        $response = $projects->getSnippet($project_id, intval($snippet_id));

        $this->assertEquals($response->getStatusCode(), 200);
    }

    public function testGetContent()
    {
        $client = $this->getClient();

        $project_id = getenv('CI_PROJECT_ID');
        $snippet_id = getenv('GITLAB_SNIPPET_ID');

        $projects = new \GitLab\Projects\Snippets($client);
        $response = $projects->getContent($project_id, intval($snippet_id));

        $this->assertEquals($response->getStatusCode(), 200);
    }

    /**
     * @group skip
     */
    public function testCreate()
    {
        $client = $this->getClient();

        $project_id = getenv('CI_PROJECT_ID');

        $projects = new \GitLab\Projects\Snippets($client);
        $response = $projects->create($project_id, 'Snippet', 'snippet.php', '<?php echo "Hola";', \GitLab\VisibilityInterface::PUBLIC);

        $this->assertEquals($response->getStatusCode(), 201);
    }

    public function testGetUserAgent()
    {
        $this->expectException(\GuzzleHttp\Exception\RequestException::class);

        $client = $this->getClient();

        $project_id = getenv('CI_PROJECT_ID');
        $snippet_id = getenv('GITLAB_SNIPPET_ID');

        $projects = new \GitLab\Projects\Snippets($client);
        $response = $projects->getUserAgent($project_id, intval($snippet_id));
    }
}
