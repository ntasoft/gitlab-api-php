<?php
declare(strict_types=1);

namespace GitLab\Test\Projects;

use PHPUnit\Framework\TestCase;

class ProjectsTest extends TestCase
{
    use \GitLab\Test\GitLabTestTrait;

    public function testGetProjects()
    {
        $client = $this->getClient();

        $projects = new \GitLab\Projects\Projects($client);
        $response = $projects->getProjects();
        $this->assertEquals($response->getStatusCode(), 200);
    }

    public function testGetUserProjects()
    {
        $client = $this->getClient();

        $user = getenv('GITLAB_USER_ID');

        $projects = new \GitLab\Projects\Projects($client);
        $response = $projects->getUserProjects($user);

        $this->assertEquals($response->getStatusCode(), 200);
    }

    public function testGetProject()
    {
        $client = $this->getClient();

        $project_id = getenv('CI_PROJECT_ID');

        $projects = new \GitLab\Projects\Projects($client);
        $response = $projects->getProject($project_id);
        $this->assertEquals($response->getStatusCode(), 200);
    }

    public function testGetProjectUsers()
    {
        $client = $this->getClient();

        $project_id = getenv('CI_PROJECT_ID');

        $projects = new \GitLab\Projects\Projects($client);
        $response = $projects->getProjectUsers($project_id);
        $this->assertEquals($response->getStatusCode(), 200);
    }

    public function testGetForks()
    {
        $this->expectException(\GuzzleHttp\Exception\RequestException::class);

        $client = $this->getClient();

        $project_id = getenv('CI_PROJECT_ID');

        $projects = new \GitLab\Projects\Projects($client);
        $response = $projects->getForks($project_id);
    }

    /**
     * @group skip
     */
    public function testStar()
    {
        $client = $this->getClient();

        $project_id = getenv('CI_PROJECT_ID');

        $projects = new \GitLab\Projects\Projects($client);
        $response = $projects->star($project_id);
        $this->assertEquals($response->getStatusCode(), 201);
    }

    /**
     * @group skip
     */
    public function testUnstar()
    {
        $client = $this->getClient();

        $project_id = getenv('CI_PROJECT_ID');

        $projects = new \GitLab\Projects\Projects($client);
        $response = $projects->unstar($project_id);
        $this->assertEquals($response->getStatusCode(), 201);
    }

    public function testGetLanguages()
    {
        $client = $this->getClient();

        $project_id = getenv('CI_PROJECT_ID');

        $projects = new \GitLab\Projects\Projects($client);
        $response = $projects->getLanguages($project_id);

        $this->assertEquals($response->getStatusCode(), 200);
    }
}
