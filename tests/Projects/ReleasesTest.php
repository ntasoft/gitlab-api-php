<?php
declare(strict_types=1);

namespace GitLab\Test\Projects;

use PHPUnit\Framework\TestCase;

class ReleasesTest extends TestCase
{
    use \GitLab\Test\GitLabTestTrait;

    public function testGetReleases()
    {
        $client = $this->getClient();

        $project_id = getenv('CI_PROJECT_ID');

        $tags = new \GitLab\Projects\Releases($client);
        $response = $tags->getReleases($project_id);

        $this->assertEquals($response->getStatusCode(), 200);
    }

    public function testGetRelease()
    {
        $this->expectException(\GuzzleHttp\Exception\RequestException::class);

        $client = $this->getClient();

        $project_id = getenv('CI_PROJECT_ID');

        $tags = new \GitLab\Projects\Releases($client);
        $response = $tags->getRelease($project_id, 'v1.0');
    }
}
