<?php
declare(strict_types=1);

namespace GitLab\Test\Projects;

use PHPUnit\Framework\TestCase;

class RepositoryFilesTest extends TestCase
{
    use \GitLab\Test\GitLabTestTrait;

    public function testGetFile()
    {
        $client = $this->getClient();

        $project_id = getenv('CI_PROJECT_ID');

        $tags = new \GitLab\Projects\RepositoryFiles($client);
        $response = $tags->getFile($project_id, 'README.md');

        $this->assertEquals($response->getStatusCode(), 200);
    }
}
