<?php
declare(strict_types=1);

namespace GitLab\Test\Projects;

use PHPUnit\Framework\TestCase;

class StatisticsTest extends TestCase
{
    use \GitLab\Test\GitLabTestTrait;

    public function testGetStatistics()
    {
        $client = $this->getClient();

        $project_id = getenv('CI_PROJECT_ID');

        $tags = new \GitLab\Projects\Statistics($client);
        $response = $tags->getStatistics($project_id);

        $this->assertEquals($response->getStatusCode(), 200);
    }
}
