<?php
declare(strict_types=1);

namespace GitLab\Test\Projects;

use PHPUnit\Framework\TestCase;

class BranchesTest extends TestCase
{
    use \GitLab\Test\GitLabTestTrait;

    public function testGetBranches()
    {
        $client = $this->getClient();

        $project_id = getenv('CI_PROJECT_ID');

        $branches = new \GitLab\Projects\Branches($client);
        $response = $branches->getBranches($project_id);

        $this->assertEquals($response->getStatusCode(), 200);
    }

    public function testGetBranch()
    {
        $client = $this->getClient();

        $project_id = getenv('CI_PROJECT_ID');

        $branches = new \GitLab\Projects\Branches($client);
        $response = $branches->getBranch($project_id, 'master');

        $this->assertEquals($response->getStatusCode(), 200);
    }
}
