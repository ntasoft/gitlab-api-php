<?php
declare(strict_types=1);

namespace GitLab\Test\Projects;

use PHPUnit\Framework\TestCase;

class ResourceTest extends TestCase
{
    use \GitLab\Test\GitLabTestTrait;

    public function testAvailable()
    {
        $client = $this->getClient();

        $projects = new \GitLab\Projects\Projects($client);
        $this->assertEquals($projects->isAvailable(), true);
    }
}
