<?php
declare(strict_types=1);

namespace GitLab\Test\Projects;

use PHPUnit\Framework\TestCase;

class TagsTest extends TestCase
{
    use \GitLab\Test\GitLabTestTrait;

    public function testGetTags()
    {
        $client = $this->getClient();

        $project_id = getenv('CI_PROJECT_ID');

        $tags = new \GitLab\Projects\Tags($client);
        $response = $tags->getTags($project_id);

        $this->assertEquals($response->getStatusCode(), 200);
    }

    public function testGetTag()
    {
        $this->expectException(\GuzzleHttp\Exception\RequestException::class);

        $client = $this->getClient();

        $project_id = getenv('CI_PROJECT_ID');

        $tags = new \GitLab\Projects\Tags($client);
        $response = $tags->getTag($project_id, 'v1.0');
    }
}
