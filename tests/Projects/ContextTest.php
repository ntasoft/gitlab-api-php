<?php
declare(strict_types=1);

namespace GitLab\Test\Projects;

use PHPUnit\Framework\TestCase;

class ContextTest extends TestCase
{
    use \GitLab\Test\GitLabTestTrait;

    public function testList()
    {
        $client = $this->getClient();

        $projects = new \GitLab\Projects\Projects($client);
        $context = $projects->getContext();

        $this->assertEquals($context, 'projects');
    }
}
