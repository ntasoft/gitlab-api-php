<?php
declare(strict_types=1);

namespace GitLab\Test\Projects;

use PHPUnit\Framework\TestCase;

class ReleaseLinksTest extends TestCase
{
    use \GitLab\Test\GitLabTestTrait;

    public function testGetLinks()
    {
        $this->expectException(\GuzzleHttp\Exception\RequestException::class);

        $client = $this->getClient();

        $project_id = getenv('CI_PROJECT_ID');

        $tags = new \GitLab\Projects\ReleaseLinks($client);
        $response = $tags->getLinks($project_id, 'v1.0');
    }

    public function testGetLink()
    {
        $this->expectException(\GuzzleHttp\Exception\RequestException::class);

        $client = $this->getClient();

        $project_id = getenv('CI_PROJECT_ID');

        $tags = new \GitLab\Projects\ReleaseLinks($client);
        $response = $tags->getLink($project_id, 'v1.0', 1);
    }
}
