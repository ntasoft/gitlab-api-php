<?php
declare(strict_types=1);

namespace GitLab\Test\Projects;

use PHPUnit\Framework\TestCase;

class RepositoriesTest extends TestCase
{
    use \GitLab\Test\GitLabTestTrait;

    public function testGetTree()
    {
        $client = $this->getClient();

        $project_id = getenv('CI_PROJECT_ID');

        $projects = new \GitLab\Projects\Repositories($client);
        $response = $projects->getTree($project_id);
        $this->assertEquals($response->getStatusCode(), 200);
    }

    public function testGetBlob()
    {
        $client = $this->getClient();

        $project_id = getenv('CI_PROJECT_ID');

        $projects = new \GitLab\Projects\Repositories($client);
        $response = $projects->getBlob($project_id, '9cfa63745746f4719006738dc9f9a7b1675855f9');
        $this->assertEquals($response->getStatusCode(), 200);
    }

    public function testGetFile()
    {
        $client = $this->getClient();

        $project_id = getenv('CI_PROJECT_ID');

        $projects = new \GitLab\Projects\Repositories($client);
        $response = $projects->getFile($project_id);
        $this->assertEquals($response->getStatusCode(), 200);
    }

    public function testGetContributors()
    {
        $client = $this->getClient();

        $project_id = getenv('CI_PROJECT_ID');

        $projects = new \GitLab\Projects\Repositories($client);
        $response = $projects->getContributors($project_id);
        $this->assertEquals($response->getStatusCode(), 200);
    }

    public function testCompare()
    {
        $client = $this->getClient();

        $project_id = getenv('CI_PROJECT_ID');
        $commit = getenv('CI_COMMIT_SHA');
        $commit_before = getenv('CI_COMMIT_BEFORE_SHA');

        $projects = new \GitLab\Projects\Repositories($client);
        $response = $projects->compare($project_id, $commit_before, $commit);
        $this->assertEquals($response->getStatusCode(), 200);
    }
}
