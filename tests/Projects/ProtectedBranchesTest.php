<?php
declare(strict_types=1);

namespace GitLab\Test\Projects;

use PHPUnit\Framework\TestCase;

class ProtectedBranchesTest extends TestCase
{
    use \GitLab\Test\GitLabTestTrait;

    public function testGetBranches()
    {
        $client = $this->getClient();

        $project_id = getenv('CI_PROJECT_ID');

        $branches = new \GitLab\Projects\ProtectedBranches($client);
        $response = $branches->getBranches($project_id);

        $this->assertEquals($response->getStatusCode(), 200);
    }
}
