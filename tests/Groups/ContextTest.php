<?php
declare(strict_types=1);

namespace GitLab\Test\Groups;

use PHPUnit\Framework\TestCase;

class ContextTest extends TestCase
{
    use \GitLab\Test\GitLabTestTrait;

    public function testList()
    {
        $client = $this->getClient();

        $groups = new \GitLab\Groups\Groups($client);
        $context = $groups->getContext();

        $this->assertEquals($context, 'groups');
    }
}
