<?php
declare(strict_types=1);

namespace GitLab\Test\Groups;

use PHPUnit\Framework\TestCase;

class GroupsTest extends TestCase
{
    use \GitLab\Test\GitLabTestTrait;

    public function testGroups()
    {
        $client = $this->getClient();

        $groups = new \GitLab\Groups\Groups($client);
        $response = $groups->getGroups();

        $this->assertEquals($response->getStatusCode(), 200);
    }

    public function testGetGroup()
    {
        $client = $this->getClient();

        $group_id = getenv('GITLAB_GROUP_ID');

        $groups = new \GitLab\Groups\Groups($client);
        $response = $groups->getGroup($group_id);

        $this->assertEquals($response->getStatusCode(), 200);
    }

    public function testGetProjects()
    {
        $client = $this->getClient();

        $group_id = getenv('GITLAB_GROUP_ID');

        $groups = new \GitLab\Groups\Groups($client);
        $response = $groups->getProjects($group_id);

        $this->assertEquals($response->getStatusCode(), 200);
    }

    public function testGetSubgroups()
    {
        $client = $this->getClient();

        $group_id = getenv('GITLAB_GROUP_ID');

        $groups = new \GitLab\Groups\Groups($client);
        $response = $groups->getSubgroups($group_id);

        $this->assertEquals($response->getStatusCode(), 200);
    }
}
