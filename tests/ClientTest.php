<?php
declare(strict_types=1);

namespace GitLab\Test;

use PHPUnit\Framework\TestCase;

class ClientTest extends TestCase
{
    use GitLabTestTrait;

    public function testUri()
    {
        $client = $this->getClient();

        $this->assertEquals($client->getBaseUrl(), 'https://gitlab.com/api/v4/');
    }
}
