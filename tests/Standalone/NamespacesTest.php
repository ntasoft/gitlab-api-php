<?php
declare(strict_types=1);

namespace GitLab\Test\Standalone;

use PHPUnit\Framework\TestCase;

class NamespacesTest extends TestCase
{
    use \GitLab\Test\GitLabTestTrait;

    public function testGetNamespaces()
    {
        $client = $this->getClient();

        $namespaces = new \GitLab\Standalone\Namespaces($client);
        $response = $namespaces->getNamespaces();

        $this->assertEquals($response->getStatusCode(), 200);
    }

    public function testSearchNamespace()
    {
        $client = $this->getClient();

        $user = getenv('GITLAB_USER_NAME');

        $namespaces = new \GitLab\Standalone\Namespaces($client);
        $response = $namespaces->searchNamespace($user);

        $this->assertEquals($response->getStatusCode(), 200);
    }

    public function testGetNamespaceById()
    {
        $client = $this->getClient();

        $namespace = getenv('CI_PROJECT_NAMESPACE');

        $namespaces = new \GitLab\Standalone\Namespaces($client);
        $response = $namespaces->getNamespaceById($namespace);

        $this->assertEquals($response->getStatusCode(), 200);
    }
}
