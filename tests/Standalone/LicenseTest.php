<?php
declare(strict_types=1);

namespace GitLab\Test\Standalone;

use PHPUnit\Framework\TestCase;

class LicenseTest extends TestCase
{
    use \GitLab\Test\GitLabTestTrait;

    public function testGetLicense()
    {
        $this->expectException(\GuzzleHttp\Exception\RequestException::class);

        $client = $this->getClient();

        $license = new \GitLab\Standalone\License($client);
        $response = $license->getLicense();
    }

    public function testGetLicenses()
    {
        $this->expectException(\GuzzleHttp\Exception\RequestException::class);

        $client = $this->getClient();

        $license = new \GitLab\Standalone\License($client);
        $response = $license->getLicenses();
    }

    public function testIsAvailable()
    {
        $client = $this->getClient();

        $license = new \GitLab\Standalone\License($client);

        $this->assertEquals($license->isAvailable(), false);
    }
}
