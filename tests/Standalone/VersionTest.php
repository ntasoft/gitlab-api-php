<?php
declare(strict_types=1);

namespace GitLab\Test\Standalone;

use PHPUnit\Framework\TestCase;

class VersionTest extends TestCase
{
    use \GitLab\Test\GitLabTestTrait;

    public function testVersion()
    {
        $client = $this->getClient();

        $version = new \GitLab\Standalone\Version($client);
        $response = $version->getVersion();

        $this->assertEquals($response->getStatusCode(), 200);
    }

    public function testIsAvailable()
    {
        $client = $this->getClient();

        $version = new \GitLab\Standalone\Version($client);

        $this->assertEquals($version->isAvailable(), true);
    }
}
