<?php
declare(strict_types=1);

namespace GitLab\Test\Standalone;

use PHPUnit\Framework\TestCase;

class LintTest extends TestCase
{
    use \GitLab\Test\GitLabTestTrait;

    public function testLint()
    {
        $client = $this->getClient();

        $content = <<<'EOD'

# Official docker image.
image: docker:latest

services:
  - docker:dind

EOD;

        $lint = new \GitLab\Standalone\Lint($client);
        $response = $lint->isValid($content);

        $this->assertEquals($response->getStatusCode(), 200);
    }
}
