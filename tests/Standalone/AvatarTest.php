<?php
declare(strict_types=1);

namespace GitLab\Test\Standalone;

use PHPUnit\Framework\TestCase;

class AvatarTest extends TestCase
{
    use \GitLab\Test\GitLabTestTrait;

    public function testGetAvatar()
    {
        $client = $this->getClient();

        $email = getenv('GITLAB_USER_EMAIL');

        $version = new \GitLab\Standalone\Avatar($client);
        $response = $version->getAvatar($email, 10);

        $this->assertEquals($response->getStatusCode(), 200);
    }
}
