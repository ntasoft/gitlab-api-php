<?php
declare(strict_types=1);

namespace GitLab\Test;

trait GitLabTestTrait
{
    public function getClient()
    {
        return new \GitLab\Client([
            'debug'   => true,
            'headers' => [
                'PRIVATE-TOKEN' => getenv('GITLAB_PRIVATE_ACCESS_TOKEN')
            ]
        ]);
    }
}
