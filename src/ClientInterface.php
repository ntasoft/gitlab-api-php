<?php
/**
 * GitLab Client
 *
 * @copyright Copyright (c) Luis A. Ochoa
 * @since     1.0.0
 * @license   https://opensource.org/licenses/MIT MIT License
 */

namespace GitLab;

interface ClientInterface
{
    /**
     * Returns the base URL that the client points towards.
     *
     * @return string
     */
    public function getBaseUrl();
}
