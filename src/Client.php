<?php
/**
 * GitLab Client
 *
 * @copyright Copyright (c) Luis A. Ochoa
 * @since     1.0.0
 * @license   https://opensource.org/licenses/MIT MIT License
 */

namespace GitLab;

use GuzzleHttp\Client as GuzzleClient;

/**
 * Client used to interact with GitLab.
 *
 * @since 1.0.0
 */
class Client implements ClientInterface
{
    /**
     * @constant GITLAB_API_URI
     */
    const GITLAB_API_URI = 'https://gitlab.com/api/v4/';

    /**
     * GuzzleHttp HTTP client.
     *
     * @var GuzzleHttp\Client HTTP client.
     */
    private $client = null;

    /**
     * The client's constructor.
     *
     * @since 1.0.0
     *
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $config['base_uri'] = $config['base_uri'] ?? Client::GITLAB_API_URI;

        $this->client = new GuzzleClient($config);
    }

    /**
     * Send a request.
     *
     * @since 1.0.0
     */
    public function request($method, $uri, array $options = [])
    {
        return $this->client->request($method, $uri, $options);
    }

    /**
     * {@inheritDoc}
     *
     * @since 1.0.0
     */
    public function getBaseUrl(): string
    {
        return (string) $this->client->getConfig('base_uri');
    }
}
