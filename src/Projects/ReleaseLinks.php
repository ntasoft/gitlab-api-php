<?php
/**
 * Release Links API
 *
 * @copyright Copyright (c) Luis A. Ochoa
 * @since     1.0.0
 * @license   https://opensource.org/licenses/MIT MIT License
 */

namespace GitLab\Projects;

use GitLab\ClientInterface;

/**
 * Release Links API
 *
 * @link https://docs.gitlab.com/ee/api/releases/links.html
 *
 * @since 1.0.0
 */
class ReleaseLinks extends ApiResource
{
    /**
     * {@inheritDoc}
     *
     * @param ClientInterface $client GitLab HTTP client.
     */
    public function __construct(ClientInterface $client)
    {
        parent::__construct($client);
    }

    /**
     * Get assets as links from a release.
     *
     * GET /projects/:id/releases/:tag_name/assets/links
     *
     * @link https://docs.gitlab.com/ee/api/releases/links.html#get-links
     *
     * @since 1.0.0
     *
     * @param mixed  $id The ID or URL-encoded path of the project.
     * @param string $tag_name The tag associated with the release.
     */
    public function getLinks($id, string $tag_name)
    {
        $project_id = is_numeric($id) ? $id : urlencode($id);

        return $this->client->request('GET', "projects/$project_id/releases/$tag_name/assets/links");
    }

    /**
     * Get an asset as a link from a release.
     *
     * GET /projects/:id/releases/:tag_name/assets/links/:link_id
     *
     * @link https://docs.gitlab.com/ee/api/releases/links.html#get-a-link
     *
     * @since 1.0.0
     *
     * @param mixed  $id The ID or URL-encoded path of the project.
     * @param string $tag_name The tag associated with the release.
     * @param int    $link_id The id of the link.
     */
    public function getLink($id, string $tag_name, int $link_id)
    {
        $project_id = is_numeric($id) ? $id : urlencode($id);

        return $this->client->request('GET', "projects/$project_id/releases/$tag_name/assets/links/$link_id");
    }

    /**
     * Create an asset as a link from a release.
     *
     * POST /projects/:id/releases/:tag_name/assets/links
     *
     * @link https://docs.gitlab.com/ee/api/releases/links.html#create-a-link
     *
     * @since 1.0.0
     *
     * @param mixed  $id The ID or URL-encoded path of the project.
     * @param string $tag_name The tag associated with the release.
     * @param string $name The name of the link.
     * @param string $url The URL of the link.
     */
    public function createLink($id, string $tag_name, string $name, string $url)
    {
        $project_id = is_numeric($id) ? $id : urlencode($id);

        $attributes = compact("name", "url");

        return $this->client->request('POST', "projects/$project_id/releases/$tag_name/assets/links", [
            'query' => $attributes
        ]);
    }

    /**
     * Update an asset as a link from a release.
     *
     * PUT /projects/:id/releases/:tag_name/assets/links/:link_id
     *
     * @link https://docs.gitlab.com/ee/api/releases/links.html#update-a-link
     *
     * @since 1.0.0
     *
     * @param mixed  $id The ID or URL-encoded path of the project.
     * @param string $tag_name The tag associated with the release.
     * @param int    $link_id The ID of the link.
     * @param string $name The name of the link.
     * @param string $url The URL of the link.
     */
    public function updateLink($id, string $tag_name, int $link_id, string $name, string $url)
    {
        $project_id = is_numeric($id) ? $id : urlencode($id);

        $attributes = compact("name", "url");

        return $this->client->request('POST', "projects/$project_id/releases/$tag_name/assets/links/$link_id", [
            'query' => $attributes
        ]);
    }

    /**
     * Delete an asset as a link from a release.
     *
     * DELETE /projects/:id/releases/:tag_name/assets/links/:link_id
     *
     * @link https://docs.gitlab.com/ee/api/releases/links.html#delete-a-link
     *
     * @since 1.0.0
     *
     * @param mixed  $id The ID or URL-encoded path of the project.
     * @param string $tag_name The tag associated with the release.
     * @param int    $link_id The ID of the link.
     */
    public function deleteLink($id, string $tag_name, int $link_id)
    {
        $project_id = is_numeric($id) ? $id : urlencode($id);

        return $this->client->request('DELETE', "projects/$project_id/releases/$tag_name/assets/links/$link_id");
    }
}
