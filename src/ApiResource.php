<?php
/**
 * Resource
 *
 * @copyright Copyright (c) Luis A. Ochoa
 * @since     1.0.0
 * @license   https://opensource.org/licenses/MIT MIT License
 */

namespace GitLab;

use GitLab\ClientInterface;

/**
 * Resource
 *
 * @since 1.0.0
 */
class ApiResource
{
    /**
     * GitLab HTTP client.
     *
     * @var GitLab\ClientInterface
     */
    protected $client = null;

    /**
     * Constructor.
     *
     * @since 1.0.0
     *
     * @param ClientInterface $client GitLab HTTP client.
     */
    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }
}
