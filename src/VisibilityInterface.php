<?php
/**
 * Visibility interface
 *
 * @copyright Copyright (c) Luis A. Ochoa
 * @since     1.0.0
 * @license   https://opensource.org/licenses/MIT MIT License
 */

namespace GitLab;

/**
 * Visibility levels
 *
 * @since 1.0.0
 */
interface VisibilityInterface
{
    const PRIVATE = 'private';

    const INTERNAL = 'internal';

    const PUBLIC = 'public';
}
