<?php 
/**
 * Access Requests API
 *
 * @copyright Copyright (c) Luis A. Ochoa
 * @since     1.0.0
 * @license   https://opensource.org/licenses/MIT MIT License
 */

namespace GitLab;

/**
 * Group and project access requests API
 * 
 * @link https://docs.gitlab.com/ee/api/access_requests.html
 *
 * @since 1.0.0
 */
trait AccessRequestsTrait
{

    /**
     * List access requests for a group or project.
     *
     * GET /groups/:id/access_requests
     * GET /projects/:id/access_requests
     *
     * @link https://docs.gitlab.com/ee/api/access_requests.html#list-access-requests-for-a-group-or-project
     *
     * @since 1.0.0
     *
     * @param mixed $id The ID or URL-encoded path of the project owned by the
     *     authenticated user
     */
    public function getAccessRequests($id)
    {
        $_id = is_numeric($id) ? $id : urlencode($id);

        $endpoint = sprintf('%s/%s/access_requests', $this->getContext(), $_id);

        return $this->client->request('GET', $endpoint);
    }

    /**
     * Requests access for the authenticated user to a group or project.
     *
     * @link https://docs.gitlab.com/ee/api/access_requests.html#request-access-to-a-group-or-project
     *
     * @since 1.0.0
     *
     * @param mixed $id The ID or URL-encoded path of the project owned by the
     *     authenticated user
     */
    public function requestAccess($id)
    {
        $_id = is_numeric($id) ? $id : urlencode($id);

        $endpoint = sprintf('%s/%s/access_requests', $this->getContext(), $_id);

        return $this->client->request('POST', $endpoint);
    }

    /**
     * Approves an access request for the given user.
     *
     * @link https://docs.gitlab.com/ee/api/access_requests.html#approve-an-access-request
     *
     * @since 1.0.0
     *
     * @param mixed $id The ID or URL-encoded path of the project owned by the
     *     authenticated user
     * @param int $user_id The user ID of the access requester
     * @param int $access_level A valid access level (defaults: 30, developer
     *     access level)
     */
    public function approve($id, int $user_id, int $access_level = 30)
    {
        $_id = is_numeric($id) ? $id : urlencode($id);

        $endpoint = sprintf('%s/%s/access_requests/%s/approve',
            $this->getContext(),
            $_id,
            $user_id
        );

        return $this->client->request('PUT', $endpoint, [
            'query' => ['access_level' => $access_level]
        ]);
    }

    /**
     * Denies an access request for the given user.
     *
     * @link https://docs.gitlab.com/ee/api/access_requests.html#deny-an-access-request
     *
     * @since 1.0.0
     *
     * @param mixed $id The ID or URL-encoded path of the project owned by the
     *     authenticated user
     * @param int $user_id The user ID of the access requester
     */
    public function deny($id, int $user_id)
    {
        $_id = is_numeric($id) ? $id : urlencode($id);

        $endpoint = sprintf('%s/%s/access_requests/%s',
            $this->getContext(),
            $_id,
            $user_id
        );

        return $this->client->request('DELETE', $endpoint);
    }
}

