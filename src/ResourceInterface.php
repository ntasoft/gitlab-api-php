<?php
/**
 * Resource interface
 *
 * @copyright Copyright (c) Luis A. Ochoa
 * @since     1.0.0
 * @license   https://opensource.org/licenses/MIT MIT License
 */

namespace GitLab;

/**
 * Resource interface
 *
 * @since 1.0.0
 */
interface ResourceInterface
{
    /**
     * Verifies if a resource is available in the current instance.
     *
     * @since 1.0.0
     */
    public function isAvailable(): bool;

    /**
     * Gets the context to which a resource belongs.
     *
     * @since 1.0.0
     */
    public function getContext(): string;
}
