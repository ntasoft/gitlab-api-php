<?php
/**
 * Resource
 *
 * @copyright Copyright (c) Luis A. Ochoa
 * @since     1.0.0
 * @license   https://opensource.org/licenses/MIT MIT License
 */

namespace GitLab\Groups;

use GitLab\ClientInterface;
use GitLab\ResourceInterface;

/**
 * Resource
 *
 * @since 1.0.0
 */
class ApiResource implements ResourceInterface
{
    /**
     * GitLab HTTP client.
     *
     * @var GitLab\ClientInterface
     */
    protected $client = null;

    /**
     * Constructor.
     *
     * @since 1.0.0
     *
     * @param ClientInterface $client GitLab HTTP client.
     */
    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * {@inheritdoc}
     *
     * @since 1.0.0
     */
    public function isAvailable(): bool
    {
        return true;
    }

    /**
     * {@inheritdoc}
     *
     * @since 1.0.0
     */
    public function getContext(): string
    {
        return 'groups';
    }
}
