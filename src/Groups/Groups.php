<?php
/**
 * Groups API
 *
 * @copyright Copyright (c) Luis A. Ochoa
 * @since     1.0.0
 * @license   https://opensource.org/licenses/MIT MIT License
 */

namespace GitLab\Groups;

use GitLab\ClientInterface;

/**
 * Groups API
 *
 * @link https://docs.gitlab.com/ee/api/groups.html
 *
 * @since 1.0.0
 */
class Groups extends ApiResource
{
    /**
     * {@inheritDoc}
     *
     * @param ClientInterface $client GitLab HTTP client.
     */
    public function __construct(ClientInterface $client)
    {
        parent::__construct($client);
    }

    /**
     * Get a list of visible groups for the authenticated user.
     *
     * GET /groups
     *
     * @link https://docs.gitlab.com/ee/api/groups.html#list-groups
     *
     * @since 1.0.0
     *
     * @param array $attributes Parameters.
     */
    public function getGroups(array $attributes = [])
    {
        return $this->client->request('GET', 'groups', [
            'query' => $attributes
        ]);
    }

    /**
     * Get a list of visible direct subgroups in this group.
     *
     * GET /groups/:id/subgroups
     *
     * @link https://docs.gitlab.com/ee/api/groups.html#list-a-groups-subgroups
     *
     * @since 1.0.0
     *
     * @param mixed $id The ID or URL-encoded path of the group of the parent group.
     * @param array $attributes Parameters.
     */
    public function getSubgroups($id, array $attributes = [])
    {
        $group_id = is_numeric($id) ? $id : urlencode($id);

        return $this->client->request('GET', "groups/$group_id/subgroups", [
            'query' => $attributes
        ]);
    }

    /**
     * List a group's projects.
     *
     * GET /groups/:id/projects
     *
     * @link https://docs.gitlab.com/ee/api/groups.html#list-a-groups-projects
     *
     * @since 1.0.0
     *
     * @param mixed $id The ID or URL-encoded path of the group owned by the authenticated user.
     * @param array $attributes Parameters.
     */
    public function getProjects($id, array $attributes = [])
    {
        $group_id = is_numeric($id) ? $id : urlencode($id);

        return $this->client->request('GET', "groups/$group_id/projects", [
            'query' => $attributes
        ]);
    }

    /**
     * Get all details of a group.
     *
     * @link https://docs.gitlab.com/ee/api/groups.html#details-of-a-group
     *
     * GET /groups/:id
     *
     * @since 1.0.0
     *
     * @param mixed $id The ID or URL-encoded path of the group owned by the authenticated user.
     * @param array $attributes Parameters.
     */
    public function getGroup($id, array $attributes = [])
    {
        $group_id = is_numeric($id) ? $id : urlencode($id);

        return $this->client->request('GET', "groups/$group_id", [
            'query' => $attributes
        ]);
    }
}
