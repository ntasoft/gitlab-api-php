<?php
/**
 * Version API
 *
 * @copyright Copyright (c) Luis A. Ochoa
 * @since     1.0.0
 * @license   https://opensource.org/licenses/MIT MIT License
 */

namespace GitLab\Standalone;

use GitLab\ClientInterface;

/**
 * Version API
 *
 * @link https://docs.gitlab.com/ee/api/version.html
 *
 * @since 1.0.0
 */
class Version extends ApiResource
{
    /**
     * {@inheritDoc}
     *
     * @param ClientInterface $client GitLab HTTP client.
     */
    public function __construct(ClientInterface $client)
    {
        parent::__construct($client);
    }

    /**
     * Retrieve version information for this GitLab instance.
     *
     * GET /version
     *
     * @since 1.0.0
     */
    public function getVersion()
    {
        return $this->client->request('GET', 'version');
    }
}
