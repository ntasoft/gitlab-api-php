<?php
/**
 * Avatar API
 *
 * @copyright Copyright (c) Luis A. Ochoa
 * @since     1.0.0
 * @license   https://opensource.org/licenses/MIT MIT License
 */

namespace GitLab\Standalone;

use GitLab\ClientInterface;

/**
 * Avatar API
 *
 * @link https://docs.gitlab.com/ee/api/avatar.html
 *
 * @since 1.0.0
 */
class Avatar extends ApiResource
{
    /**
     * {@inheritDoc}
     *
     * @param ClientInterface $client GitLab HTTP client.
     */
    public function __construct(ClientInterface $client)
    {
        parent::__construct($client);
    }

    /**
     * Get a single avatar URL.
     *
     * GET /avatar
     *
     * @since 1.0.0
     *
     * @param string $email Public email address of the user.
     * @param int $size Single pixel dimension (since images are squares).
     */
    public function getAvatar(string $email, int $size = 48 )
    {
        $query = compact("email", "size");

        return $this->client->request('GET', 'avatar', [
            'query' => $query
        ]);
    }
}
